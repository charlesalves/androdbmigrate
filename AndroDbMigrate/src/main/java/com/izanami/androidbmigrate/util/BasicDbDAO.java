package com.izanami.androidbmigrate.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.izanami.androidbmigrate.helper.MigrationDbHelper;

public abstract class BasicDbDAO<Tipo extends BasicDbDAO.Insertable> {

	private MigrationDbHelper databaseAdapter;
	private SQLiteDatabase database;

	public BasicDbDAO(MigrationDbHelper databaseAdapter) {
		this.databaseAdapter = databaseAdapter;
		this.database = databaseAdapter.getWritableDatabase();
	}

	public void close() {
		database.close();
		databaseAdapter.close();
	}

	public long salvar(Tipo insertable) {

		if (insertable.getId() == 0) {
			long id = getDatabase().insert(getTabela(), null,
					getContentValues(insertable));
			insertable.setId(id);
		} else {
			getDatabase().update(getTabela(), getContentValues(insertable),
					getWhereIdQuery(),
					StringUtils.toArray(String.valueOf(insertable.getId())));
		}

		return insertable.getId();
	}

	public void salvarTodos(Collection<Tipo> insertables) {

		if (insertables == null || insertables.isEmpty()) {
			return;
		}

		getDatabase().beginTransaction();

		for (Tipo insertable : insertables) {
			salvar(insertable);
		}

		getDatabase().setTransactionSuccessful();
		getDatabase().endTransaction();
	}

	public Tipo find(long id) {
		return findByWhere(getWhereIdQuery(),
				StringUtils.toArray(String.valueOf(id)), null, null);
	}

	public List<Tipo> listAll() {
		return listByWhere(null, null, null, null);
	}

	public void delete(long id) {

		database.delete(getTabela(), getWhereIdQuery(),
				StringUtils.toArray(String.valueOf(id)));

	};

	public void delete(Tipo insertable) {
		delete(insertable.getId());
	}

	public MigrationDbHelper getDatabaseAdapter() {
		return databaseAdapter;
	}

	protected SQLiteDatabase getDatabase() {
		return database;
	}

	protected List<Tipo> getResultsByCursor(Cursor cursor) {
		List<Tipo> result = new ArrayList<Tipo>();

		while (cursor.moveToNext()) {
			result.add(getInstanceByCursor(cursor));
		}

		cursor.close();

		return result;
	}

	protected Tipo getUniqueResultByCursor(Cursor cursor) {
		Tipo retorno = null;
		if (cursor.moveToFirst()) {
			retorno = getInstanceByCursor(cursor);
		}

		cursor.close();

		return retorno;
	}

	protected abstract ContentValues getContentValues(Tipo insertable);

	protected abstract String getTabela();

	protected abstract String getColunaId();

	protected abstract String[] getColunas();

	protected abstract Tipo getInstanceByCursor(Cursor cursor);

	protected Tipo findByWhere(String section, String[] sectionArgs,
			String groupBy, String orderBy) {
		Cursor cursor = createCursorWhere(section, sectionArgs, groupBy,
				orderBy);

		return getUniqueResultByCursor(cursor);
	}

	protected List<Tipo> listByWhere(String section, String[] sectionArgs,
			String groupBy, String orderBy) {
		Cursor cursor = createCursorWhere(section, sectionArgs, groupBy,
				orderBy);

		return getResultsByCursor(cursor);
	}

	private Cursor createCursorWhere(String section, String[] sectionArgs,
			String groupBy, String orderBy) {
		return getDatabase().query(getTabela(), getColunas(), section,
				sectionArgs, groupBy, null, orderBy);
	}

	private String getWhereIdQuery() {
		return getColunaId() + " = ?";
	}

	public interface Insertable {

		long getId();

		void setId(long id);

	}

}
