package com.izanami.androidbmigrate.util;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;
import dalvik.system.DexFile;
import dalvik.system.PathClassLoader;

public class Reflections {

	String packageScan;
	Context context;

	public Reflections(String packageScan, Context context) {
		this.packageScan = packageScan;
		this.context = context;
	}

	@SuppressWarnings("unchecked")
	public <TipoClasse> Set<Class<? extends TipoClasse>> getSubTypesOf(
			Class<TipoClasse> clazz) {

		if (clazz == null) {
			throw new NullPointerException();
		}

		Set<Class<? extends TipoClasse>> classes = new HashSet<>();

		try {
			String apk = context.getPackageManager().getApplicationInfo(
					context.getPackageName(), 0).sourceDir;
			DexFile dexFile = new DexFile(apk);

			PathClassLoader classLoader = new PathClassLoader(apk, Thread
					.currentThread().getContextClassLoader());

			Enumeration<String> entries = dexFile.entries();

			while (entries.hasMoreElements()) {
				String string = (String) entries.nextElement();

				if (string.startsWith(packageScan)) {

					Log.v(getClass().getSimpleName(), string);

					Class<?> entryClass = classLoader.loadClass(string);
					if (entryClass != null) {

						for (Class<?> class1 : entryClass.getInterfaces()) {
							if (class1.equals(clazz)) {
								classes.add((Class<TipoClasse>) entryClass);
							}
						}
					}
				}
			}

		} catch (NameNotFoundException | IOException | ClassNotFoundException e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		}

		return classes;
	}
}
