package com.izanami.androidbmigrate.helper;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.izanami.androidbmigrate.migrations.ClassMigration;
import com.izanami.androidbmigrate.migrations.DbMigration;
import com.izanami.androidbmigrate.migrations.FileMigration;
import com.izanami.androidbmigrate.migrations.Migration;
import com.izanami.androidbmigrate.util.Reflections;
import com.izanami.androidbmigrate.util.StringUtils;
import com.izanami.androidbmigrate.util.TableBuilder;

public class MigrationDbHelper extends SQLiteOpenHelper {

	private static final String COLLUMN_VERSAO = "versao";
	private static final String DB_VERSION_TABLE = "DB_VERSION";

	private Context context;

	private String packageScan;

	public MigrationDbHelper(Context context, String name,
			CursorFactory factory, int version,
			DatabaseErrorHandler errorHandler, String packageScan) {
		super(context, name, factory, version, errorHandler);
		this.context = context;
		this.packageScan = packageScan;
	}

	public MigrationDbHelper(Context context, String name,
			CursorFactory factory, int version, String packageScan) {
		super(context, name, factory, version);
		this.context = context;
		this.packageScan = packageScan;
	}

	@Override
	public void onCreate(SQLiteDatabase database) {

		try {
			TableBuilder builder = new TableBuilder(DB_VERSION_TABLE)
					.setPrimaryKey("_id", TableBuilder.INTEGER, true)
					.addColuna(COLLUMN_VERSAO, TableBuilder.TEXT, true);

			database.execSQL(builder.toString());

		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), e.getMessage(), e);
		}

		migrate(database);

	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		migrate(database);
	}

	public void migrate(SQLiteDatabase database) {

		String version = updateVersion(database);

		if (version != null) {
			Log.i(getClass().getSimpleName(), "Iniciando Migracao - " + version);
		} else {
			Log.i(getClass().getSimpleName(), "Iniciando Migracao de criacao ");
		}

		Reflections reflections = new Reflections(packageScan, context);

		Set<Class<? extends DbMigration>> migrationsClass = reflections
				.getSubTypesOf(DbMigration.class);

		Set<Migration> migrations = new TreeSet<Migration>();

		for (Class<? extends DbMigration> migrationClass : migrationsClass) {
			Log.d(getClass().getSimpleName(), "Classes de migracao "
					+ migrationClass.getName());
			if (version == null
					|| migrationClass.getSimpleName().compareTo(version) > 0) {
				Migration migration = new ClassMigration(migrationClass);
				migrations.add(migration);
			}

		}

		AssetManager assetManager = context.getAssets();

		try {

			String[] arquivosMigracao = assetManager.list("db");

			for (String fileMigrationName : arquivosMigracao) {
				Log.d(getClass().getSimpleName(), "Arquivos de migracao "
						+ fileMigrationName);

				Migration migration = new FileMigration(fileMigrationName);

				if (migration.isValid(version)) {
					migrations.add(migration);
				}

			}
		} catch (IOException e) {
		}

		for (Migration migration : migrations) {
			Log.d(getClass().getSimpleName(), "Realizando migração - "
					+ migration.getName());
			migration.migrate(context, database);
		}

		Log.i(getClass().getSimpleName(), "Finalizando Migracao");

	}

	private String updateVersion(SQLiteDatabase database) {
		Cursor cursor = database.query(DB_VERSION_TABLE,
				StringUtils.toArray(COLLUMN_VERSAO), null, null, null, null,
				"_id DESC", "1");

		String versao = null;

		if (cursor.moveToFirst()) {
			versao = cursor.getString(0);
		}

		return versao;
	}

}
