package com.izanami.androidbmigrate.migrations;

import android.database.sqlite.SQLiteDatabase;

public interface DbMigration {
	
	void migrate(SQLiteDatabase db);

}
