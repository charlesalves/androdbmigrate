package com.izanami.androidbmigrate.migrations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class FileMigration extends Migration {

	private String fileName;

	public FileMigration(String fileName) {
		super(getNomeSemExtencao(fileName));
		this.fileName = fileName;
	}

	private static String getNomeSemExtencao(String fileName) {
		return fileName.substring(0, fileName.indexOf('.'));
	}

	@Override
	protected void onMigrate(Context context, SQLiteDatabase db) {

		try {
			InputStream stream = null;

			AssetManager assetManager = context.getAssets();
			stream = assetManager.open("db/" + fileName);

			db.beginTransaction();

			InputStreamReader isr = new InputStreamReader(stream);
			BufferedReader bufferedReader = new BufferedReader(isr);

			String linha = null;

			while ((linha = bufferedReader.readLine()) != null) {
				Log.v(getClass().getSimpleName(), linha);
				db.execSQL(linha);
			}

			db.setTransactionSuccessful();
			db.endTransaction();

		} catch (IOException e) {
			Log.e(getClass().getSimpleName(),
					"Problemas ao migrar o arquivo - " + getName(), e);
		}

	}

}
