package com.izanami.androidbmigrate.migrations;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ClassMigration extends Migration {

	private Class<? extends DbMigration> clazz;

	public ClassMigration(Class<? extends DbMigration> clazz) {
		super(clazz.getSimpleName());
		this.clazz = clazz;
	}

	@Override
	protected void onMigrate(Context context, SQLiteDatabase db) {

		try {
			clazz.newInstance().migrate(db);
		} catch (InstantiationException | IllegalAccessException e) {
			Log.e(getClass().getSimpleName(),
					"Erro ao tentar migrar a classe - " + getName(), e);
		}

	}
}
