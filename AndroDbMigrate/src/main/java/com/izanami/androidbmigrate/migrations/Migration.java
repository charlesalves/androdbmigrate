package com.izanami.androidbmigrate.migrations;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public abstract class Migration implements Comparable<Migration> {

	private String name;

	public void migrate(Context context, SQLiteDatabase db) {

		ContentValues contentValues = new ContentValues();

		contentValues.put("versao", name);
		
		db.insert("DB_VERSION", null, contentValues);

		onMigrate(context, db);
		
		
	}

	protected abstract void onMigrate(Context context, SQLiteDatabase db);

	public Migration(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public int compareTo(Migration o) {
		return name.compareTo(o.getName());
	}

	public boolean isValid(String dbVersion) {
		if (dbVersion != null) {
			return name.compareTo(dbVersion) > 0;
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Migration other = (Migration) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
